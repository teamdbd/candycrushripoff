package com.teamdbd.helper;

import android.graphics.Color;

import com.teamdbd.dataStructure.Cell;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by bepala on 2017-01-26.
 */

public class Helper {
    private static final int[] colors = new int[]{Color.RED, Color.MAGENTA, Color.GREEN, Color.BLUE, Color.YELLOW, Color.rgb(255, 102, 0)};
    static public Random rand = new Random();

    static public int getRandomColor(int colorNb) {
        return colors[rand.nextInt(colorNb)];
    }

    public static ArrayList<Integer> getAllColors(int nbOfColors) {
        ArrayList<Integer> allColors = new ArrayList<>();

        for (int i = 0; i < nbOfColors; ++i) {
            allColors.add(colors[i]);
        }

        return allColors;
    }

    public static char getColorChar(Cell cell) {
        char colorChar = '.';
        switch (cell.getColor()) {
            case Color.RED:
                colorChar = 'b';
                break;
            case Color.MAGENTA:
                colorChar = 'm';
                break;
            case Color.GREEN:
                colorChar = 'g';
                break;
            case Color.BLUE:
                colorChar = 'u';
                break;
            case Color.YELLOW:
                colorChar = 'y';
                break;
            case -39424:
                colorChar = 'w';
                break;
            default:
                break;
        }

        return colorChar;
    }
}
