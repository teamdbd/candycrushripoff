package com.teamdbd.dataStructure;

/**
 * Created by bepala on 2017-01-30.
 */

/**
 * Un espece de profile qui survie la duree de l'application
 */
public class Profile  {
    static public Level[] Levels = new Level[]{
            new Level(8,5,6,800,6,true),
            new Level(8,6,10,1200,6,false),
            new Level(7,7,10,1400,5,false),
            new Level(7,8,10,1800,5,false),
    };
}