package com.teamdbd.dataStructure;

import android.graphics.PorterDuff;
import android.widget.TextView;

/**
 * Created by bepala on 2017-01-31.
 */

public class Cell {
    private int Color;
    private TextView view;
    public boolean exploded;

    public int getColor() {
        return Color;
    }

    public void setColor(int color) {
        Color = color;
        if (view != null) {
            view.getBackground().setColorFilter(color, PorterDuff.Mode.MULTIPLY);
        }
    }

    public Cell(int color) {
        setColor(color);
    }

    public void setView(TextView view) {
        this.view = view;
    }
}
