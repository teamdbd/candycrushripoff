package com.teamdbd.dataStructure;

import com.teamdbd.ccripoff.BuildConfig;
import com.teamdbd.helper.Helper;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by bepala on 2017-01-31.
 */

public class Grid {
    public Cell getCellAt(int x, int y) {
        return cells[x][y];
    }

    public int checkExplosion() {
        int scoreDone = 0;

        for (int x = 0; x < getWidth(); ++x) {
            for (int y = 0; y < getHeight(); ++y) {
                //on check juste a droite et en bas

                if (x < getWidth() - 2) {
                    if (checkRight(x, y, 5)) {
                        scoreDone += 300;
                        for (int i = 0; i < 5; ++i) {
                            cells[x + i][y].exploded = true;
                        }
                    } else if (checkRight(x, y, 4)) {
                        scoreDone += 200;
                        for (int i = 0; i < 4; ++i) {
                            cells[x + i][y].exploded = true;
                        }
                    } else if (checkRight(x, y, 3)) {
                        scoreDone += 100;
                        for (int i = 0; i < 3; ++i) {
                            cells[x + i][y].exploded = true;
                        }
                    }
                }
                if (y < getHeight() - 2) {
                    if (checkDown(x, y, 5)) {
                        scoreDone += 300;
                        for (int i = 0; i < 5; ++i) {
                            cells[x][y + i].exploded = true;
                        }
                    } else if (checkDown(x, y, 4)) {
                        scoreDone += 200;
                        for (int i = 0; i < 4; ++i) {
                            cells[x][y + i].exploded = true;
                        }
                    } else if (checkDown(x, y, 3)) {
                        scoreDone += 100;
                        for (int i = 0; i < 3; ++i) {
                            cells[x][y + i].exploded = true;
                        }
                    }
                }
            }
        }

        if (BuildConfig.DEBUG) {
            for (int x = 0; x < getWidth(); ++x) {
                for (int y = 0; y < getHeight(); ++y) {
                    if (cells[x][y].exploded) {
                        System.out.println(x + ":" + y + ":" + Helper.getColorChar(getCellAt(x, y)) + " exploded");
                    }
                }
            }
        }

        //on part en bas a gauche et on descend jusqu'a ce qu'il n'y aille plus d'exploded
        for (int x = 0; x < getWidth(); ++x) {
            boolean rowIsOk = false;
            while (rowIsOk == false) {
                rowIsOk = true;
                for (int y = getHeight() - 1; y >= 0; --y) {
                    if (cells[x][y].exploded) {
                        rowIsOk = false;

                        if (y == 0) {
                            regenerate(x, y);
                        } else {
                            swapCells(cells[x][y], cells[x][y - 1]);
                        }
                    }

                }
            }
        }


        return scoreDone;
    }

    private void regenerate(int x, int y) {
        //on ne doit pas mettre 3 blocs de meme couleur d'affile au debut
        ArrayList<Integer> possibleColors = Helper.getAllColors(getNumOfColors());

        ArrayList<PossibilityResult> possibilityResultList = canHaveRow(x, y);

        for (PossibilityResult p : possibilityResultList) {
            int indexOf = possibleColors.indexOf(p.color);
            if (indexOf != -1) {
                possibleColors.remove(indexOf);
            }
        }

        cells[x][y].setColor(possibleColors.get(Helper.rand.nextInt(possibleColors.size())));
        cells[x][y].exploded = false;
    }

    private boolean checkRight(int x, int y, int length) {
        boolean hasExplosion = length == 1;

        if (x <= getWidth() - length) {
            for (int i = 0; i < length - 1; ++i) {
                hasExplosion = cells[x + i][y].getColor() == cells[x + i + 1][y].getColor();
                if (!hasExplosion) {
                    break;
                }
            }
        }

        return hasExplosion;
    }

    private boolean checkDown(int x, int y, int length) {
        boolean hasExplosion = length == 1;

        if (y <= getHeight() - length) {
            for (int j = 0; j < length - 1; ++j) {
                hasExplosion = (cells[x][y + j].getColor() == cells[x][y + j + 1].getColor());
                if (!hasExplosion) {
                    break;
                }
            }
        }

        return hasExplosion;
    }

    public void swapCells(Cell startCell, Cell endCell) {
        boolean tempExploded = startCell.exploded;
        int tempColor = startCell.getColor();


        startCell.setColor(endCell.getColor());
        startCell.exploded = endCell.exploded;

        endCell.setColor(tempColor);
        endCell.exploded = tempExploded;

    }

    private enum Directions {LEFT, RIGHT, UP, DOWN}

    ;

    private int width;
    private int height;
    private int numOfColors;

    public Cell[][] cells;
    private Cell[][] cleanCells;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


    public int getNumOfColors() {
        return numOfColors;
    }

    public void setNumOfColors(int numOfColors) {
        this.numOfColors = numOfColors;
    }

    public Grid(int width, int height, int numOfColors) {

        setWidth(width);
        setHeight(height);
        setNumOfColors(numOfColors);
    }


    public void generateGrid() {
        cleanCells = new Cell[getWidth()][getHeight()];
        cells = new Cell[getWidth()][getHeight()];

        for (int x = 0; x < getWidth(); ++x) {
            for (int y = 0; y < getHeight(); ++y) {
                cells[x][y] = new Cell(-1);
            }
        }

        for (int x = 0; x < getWidth(); ++x) {
            for (int y = 0; y < getHeight(); ++y) {
                //on ne doit pas mettre 3 blocs de meme couleur d'affile au debut
                ArrayList<Integer> possibleColors = Helper.getAllColors(getNumOfColors());

                ArrayList<PossibilityResult> possibilityResultList = canHaveRow(x, y);

                for (PossibilityResult p : possibilityResultList) {
                    int indexOf = possibleColors.indexOf(p.color);
                    if (indexOf != -1) {
                        possibleColors.remove(indexOf);
                    }
                }

                cleanCells[x][y] = new Cell(possibleColors.get(Helper.rand.nextInt(possibleColors.size())));
                cells[x][y] = new Cell(cleanCells[x][y].getColor());
            }
        }
    }

    private ArrayList<PossibilityResult> canHaveRow(int x, int y) {
        ArrayList<PossibilityResult> possibilityResults = new ArrayList<>();


        //check left
        if (x >= 2) {
            if (cells[x - 1][y].getColor() == cells[x - 2][y].getColor()) {
                possibilityResults.add(new PossibilityResult(Directions.LEFT, cells[x - 1][y].getColor()));
            }
        }

        //check right
        if (x < getWidth() - 2) {
            if (cells[x + 1][y].getColor() == cells[x + 2][y].getColor()) {
                possibilityResults.add(new PossibilityResult(Directions.RIGHT, cells[x + 1][y].getColor()));
            }
        }

        //check up
        if (y >= 2) {
            if (cells[x][y - 1].getColor() == cells[x][y - 2].getColor()) {
                possibilityResults.add(new PossibilityResult(Directions.UP, cells[x][y - 1].getColor()));
            }
        }

        //check down
        if (y < getHeight() - 2) {
            if (cells[x][y + 1].getColor() == cells[x][y + 2].getColor()) {
                possibilityResults.add(new PossibilityResult(Directions.DOWN, cells[x][y + 1].getColor()));
            }
        }

        return possibilityResults;
    }

    private class PossibilityResult {
        public Directions direction;
        public int color;

        public PossibilityResult(Directions direction, int color) {
            this.color = color;
            this.direction = direction;
        }
    }

}
