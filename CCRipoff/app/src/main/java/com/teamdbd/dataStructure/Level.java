package com.teamdbd.dataStructure;

import com.teamdbd.helper.Helper;

/**
 * Created by bepala on 2017-01-30.
 */

public class Level {
    private boolean isUnlock;
    private int maxTurn;
    private Grid grid;
    private int goalScore;
    public int currentTurn = 0;
    public int currentScore = 0;

    public int getNbTurnLeft() {
        return maxTurn - currentTurn;
    }

    public Level(int width, int height, int maxTurn, int goalScore, int numOfColors, boolean isUnlock) {
        setUnlock(isUnlock);
        setGrid(new Grid(width, height, numOfColors));

        setMaxTurn(maxTurn);
        setGoalScore(goalScore);
    }

    public boolean isUnlock() {
        return isUnlock;
    }

    public void setUnlock(boolean unlock) {
        isUnlock = unlock;
    }


    public void setMaxTurn(int maxTurn) {
        this.maxTurn = maxTurn;
    }

    public int getGoalScore() {
        return goalScore;
    }

    public void setGoalScore(int goalScore) {
        this.goalScore = goalScore;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public void swapCells(Cell startCell, Cell endCell) {
        int lastScore = 0;
        int scoreDone = 0;
        int combo = 0;
        grid.swapCells(startCell, endCell);

        //pour afficher ce qui se passe
        if (com.teamdbd.ccripoff.BuildConfig.DEBUG) {
            System.out.println("********************************************");
            System.out.println();
            OutputGrid();
        }
        //on regarde si on fait des points
        scoreDone += grid.checkExplosion();

        //sinon on remet les cases a leur état initial
        if (scoreDone == 0) {
            grid.swapCells(startCell, endCell);
        } else {
            //on fait les explosions et on gere les combo
            ++currentTurn;
            while (lastScore != scoreDone) {
                combo++;
                System.out.println();
                OutputGrid();
                lastScore = scoreDone;
                scoreDone += (grid.checkExplosion() * (combo + 1));
            }
        }
        currentScore += scoreDone;
    }


    private void OutputGrid() {
        System.out.print(" ");
        for (int x = 0; x < grid.getWidth(); ++x) {
            System.out.print(x);
        }
        System.out.println();
        for (int y = 0; y < grid.getHeight(); ++y) {
            System.out.print(y);
            for (int x = 0; x < grid.getWidth(); ++x) {
                System.out.print(Helper.getColorChar(grid.getCellAt(x, y)));
            }
            System.out.println();
        }
    }

    public void reset() {
        currentScore = 0;
        currentTurn = 0;
    }
}
