package com.teamdbd.ccripoff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Home_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home__screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button startButton = (Button) findViewById(R.id.button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Home_Screen", "Aller vers Démarrer");
                Intent demarrer = new Intent(Home_Screen.this, Select_Level.class);
                startActivity(demarrer);
            }
        });

        Button rulesButton = (Button) findViewById(R.id.button2);
        rulesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Home_Screen", "Aller vers Règles du jeu");
                Intent rules = new Intent(Home_Screen.this, Rules_Screen.class);
                startActivity(rules);
            }
        });

        Button exitButton = (Button) findViewById(R.id.button3);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Home_Screen", "Sortie de l'application");
                finish();
                System.exit(0);
            }
        });
    }
}
