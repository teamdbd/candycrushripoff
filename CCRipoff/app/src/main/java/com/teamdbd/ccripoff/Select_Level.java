package com.teamdbd.ccripoff;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.teamdbd.dataStructure.Profile;

/**
 * Created by benjamin on 26/01/2017.
 */

public class Select_Level extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_selection);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int[] btnIds = {R.id.btnNiv1, R.id.btnNiv2, R.id.btnNiv3, R.id.btnNiv4};

        for (int i = 0; i < btnIds.length; ++i) {
            final int lvl = i;
            Button startButton = (Button) findViewById(btnIds[i]);
            startButton.setEnabled(Profile.Levels[i].isUnlock());
            startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Level_Select", "Aller vers Démarrer");
                    Intent demarrer = new Intent(Select_Level.this, Level_Screen.class);
                    demarrer.putExtra("Level_Screen", lvl);
                    startActivity(demarrer);
                }
            });

        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setContentView(R.layout.level_selection);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int[] btnIds = {R.id.btnNiv1, R.id.btnNiv2, R.id.btnNiv3, R.id.btnNiv4};

        for (int i = 0; i < btnIds.length; ++i) {
            final int lvl = i;
            Button startButton = (Button) findViewById(btnIds[i]);
            startButton.setEnabled(Profile.Levels[i].isUnlock());
            startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Level_Select", "Aller vers Démarrer");
                    Intent demarrer = new Intent(Select_Level.this, Level_Screen.class);
                    demarrer.putExtra("Level_Screen", lvl);
                    startActivity(demarrer);
                }
            });
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Select_Level Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
