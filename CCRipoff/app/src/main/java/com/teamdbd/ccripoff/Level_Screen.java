package com.teamdbd.ccripoff;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.teamdbd.dataStructure.Cell;
import com.teamdbd.dataStructure.Profile;
import com.teamdbd.helper.Helper;

public class Level_Screen extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    Cell startCell;
    Cell endCell;
    int startX, startY, endX, endY;

    com.teamdbd.dataStructure.Level level;
    final Level_Screen tmp = this;
    boolean measured = false;

    int cellWdith, cellHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        int levelNum = getIntent().getExtras().getInt("Level_Screen");
        this.setTitle("Level " + (levelNum + 1));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final GridLayout gridJeu = (GridLayout) findViewById(R.id.gridJeu);

        android.view.ViewTreeObserver vto = gridJeu.getViewTreeObserver();
        //solution un peu overkill parcequ'on utilise gridlayout avec API < 21
        vto.addOnGlobalLayoutListener(new android.view.ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                if (!measured) {
                    measured = true;
                    int levelNum = getIntent().getExtras().getInt("Level_Screen");
                    level = Profile.Levels[levelNum];

                    //on set le grillage selon le niveau
                    gridJeu.setRowCount(level.getGrid().getHeight());
                    gridJeu.setColumnCount(level.getGrid().getWidth());

                    //calcule la taille des cellule.
                    //A Partir de l'API 21 on aurait pu mettre un weight au row/column de la grid
                    //et on aurait eu le meme resultat
                    cellHeight = (gridJeu.getHeight() / level.getGrid().getHeight());
                    cellWdith = gridJeu.getWidth() / level.getGrid().getWidth();


                    //pour tester avec la meme grid
                    if (BuildConfig.DEBUG) {
                        int seed = Helper.rand.nextInt();
                        System.out.println("Seed: " + seed);
                        Helper.rand.setSeed(seed);
                    }
                    level.getGrid().generateGrid();

                    updateHUD();

                    for (int x = 0; x < level.getGrid().getWidth(); ++x) {
                        for (int y = 0; y < level.getGrid().getHeight(); ++y) {

                            TextView viewTemp = new TextView(tmp);
                            Pair<Integer, Integer> coord = new Pair(x, y);
                            viewTemp.setTag(coord);
                            viewTemp.setBackground(getResources().getDrawable(R.drawable.circle));
                            viewTemp.getBackground().setColorFilter(level.getGrid().cells[x][y].getColor(), PorterDuff.Mode.MULTIPLY);


                            viewTemp.setHeight(Math.min(cellHeight, cellWdith));
                            viewTemp.setWidth(Math.min(cellHeight, cellWdith));

                            level.getGrid().getCellAt(x, y).setView(viewTemp);

                            GridLayout.Spec rowSpan = GridLayout.spec(y, 1);
                            GridLayout.Spec colSpan = GridLayout.spec(x, 1);

                            if (BuildConfig.DEBUG) {
                                //centrer le texte
                                viewTemp.setGravity(1 | 16);
                                viewTemp.setText(x + ":" + y);
                            }

                            GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(
                                    rowSpan, colSpan);

                            //centrer le cercle dans sa case
                            int widthOffset = ((gridJeu.getWidth() / level.getGrid().getWidth()) - Math.min(cellHeight, cellWdith)) / 2;
                            int heightOffset = ((gridJeu.getHeight() / level.getGrid().getHeight()) - Math.min(cellHeight, cellWdith)) / 2;
                            gridParam.setMargins(widthOffset, heightOffset, widthOffset, heightOffset);

                            gridJeu.addView(viewTemp, gridParam);
                        }
                    }

                }

                gridJeu.setOnTouchListener(btnTouchListener);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        level.reset();
    }

    /**
     * Fonction qui suit le drag de l'utilisateur
     */
    private View.OnTouchListener btnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            try {
                switch (motionEvent.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = (int) motionEvent.getX() / cellWdith;
                        startY = (int) motionEvent.getY() / cellHeight;
                        startCell = level.getGrid().getCellAt(startX, startY);
                        break;
                    case MotionEvent.ACTION_UP:
                        endX = (int) motionEvent.getX() / cellWdith;
                        endY = (int) motionEvent.getY() / cellHeight;
                        endCell = level.getGrid().getCellAt(endX, endY);

                        int dX = Math.abs(startX - endX);
                        int dY = Math.abs(startY - endY);

                        if (dX + dY == 1) {
                            level.swapCells(startCell, endCell);

                            updateHUD();
                            endOfTurnCheck();
                        }

                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
            }


            return true;
        }
    };

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.button_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                View popUpView = findViewById(R.id.action_settings);
                PopupMenu popupMenu = new PopupMenu(Level_Screen.this, popUpView);
                popupMenu.setOnMenuItemClickListener(Level_Screen.this);
                popupMenu.inflate(R.menu.popup_menu);
                popupMenu.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.retry:
                Intent intentRetry = getIntent();
                finish();
                resetLevel();
                updateHUD();
                startActivity(intentRetry);
                return true;
            case R.id.level_selection:
                finish();
                return true;
            case R.id.quit:
                final Context context = this;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Quitter CCRipOff ?");

                alertDialogBuilder
                        .setMessage("Clique sur oui pour quitter !")
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finishAffinity();
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                return true;
        }
        return true;
    }

    private void resetLevel() {
        level.reset();
    }

    private void updateHUD() {
        TextView tV = (TextView) findViewById(R.id.txtScore);
        tV.setText("Score :" + level.currentScore);
        tV = (TextView) findViewById(R.id.txtNbCoup);
        tV.setText("Coups :" + level.getNbTurnLeft());


    }

    private void endOfTurnCheck() {
        if (level.getNbTurnLeft() <= 0) {

            //unlock next level
            if (level.currentScore >= level.getGoalScore()) {
                final int currentLevel = getIntent().getExtras().getInt("Level_Screen");
                int nextLevel = Math.min(currentLevel + 1, Profile.Levels.length);

                if (currentLevel == Profile.Levels.length - 1) {
                    final Context context = this;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("BRAVO !!!");


                    alertDialogBuilder
                            .setMessage("Vous avez eu " + level.currentScore + " / " + level.getGoalScore() + " points !\nVoulez avez battu le jeu, voulez vous recommencer ?")
                            .setCancelable(false)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent nextLevel = new Intent(Level_Screen.this, Level_Screen.class);
                                    nextLevel.putExtra("Level_Screen", currentLevel);
                                    level.reset();
                                    startActivity(nextLevel);
                                }
                            })
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent selectLevel = new Intent(Level_Screen.this, Select_Level.class);
                                    startActivity(selectLevel);
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    Profile.Levels[nextLevel].setUnlock(true);

                    final Context context = this;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("BRAVO !!!");


                    alertDialogBuilder
                            .setMessage("Vous avez eu " + level.currentScore + " / " + level.getGoalScore() + " points !\nVoulez vous passer au niveau suivant ?")
                            .setCancelable(false)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent nextLevel = new Intent(Level_Screen.this, Level_Screen.class);
                                    nextLevel.putExtra("Level_Screen", (getIntent().getExtras().getInt("Level_Screen") + 1));
                                    startActivity(nextLevel);
                                }
                            })
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent selectLevel = new Intent(Level_Screen.this, Select_Level.class);
                                    startActivity(selectLevel);
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }

            } else {
                final Context context = this;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("PERDU :(");

                alertDialogBuilder
                        .setMessage("Vous avez eu " + level.currentScore + "/" + level.getGoalScore() + "points.\nVoulez vous recommencer le niveau ?")
                        .setCancelable(false)
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intentRetry = getIntent();
                                finish();
                                resetLevel();
                                updateHUD();
                                startActivity(intentRetry);
                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent selectLevel = new Intent(Level_Screen.this, Select_Level.class);
                                startActivity(selectLevel);
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }
    }
}
